$(document).ready(function(){
    var options = {
        nextButton: true,
        prevButton: true,
        showNextButtonOnInit:true,
        showPreviousButtonOnInit:true,
        animateStartingFrameIn: true,
        cycle:true,
        reverseAnimationsWhenNavigatingBackwards:true,
        autoPlay:false,
        swipeNavigation: true,
        swipeThreshold:10,
        swipeEvents: {left: "prev", right: "next", up: false, down: false},
        navigationSkip:false,
        preloader: true,
        pauseOnHover: false,
        preloadTheseFrames: [1],
        preloadTheseImages: [
            "images/tn-model1.png",
            "images/tn-model2.png",
            "images/tn-model3.png"
        ]
    };
    
   
    
    /*var sequence1 = $("#sequence1").sequence(options).data("sequence");*/
    var sequence2 = $("#sequence2").sequence(options).data("sequence");
    var sequence3 = $("#sequence3").sequence(options).data("sequence");
    var sequence4 = $("#sequence4").sequence(options).data("sequence");
    var sequence5 = $("#sequence5").sequence(options).data("sequence");
    var sequence6 = $("#sequence6").sequence(options).data("sequence");
    var sequence7 = $("#sequence7").sequence(options).data("sequence");

    /*sequence1.afterLoaded = function() {
        $("#sequence-theme1 .nav").fadeIn(100);
        $("#sequence-theme1 .nav li:nth-child("+(sequence1.settings.startingFrameID)+") img").addClass("active");
    }*/
     sequence2.afterLoaded = function() {
        $("#sequence-theme2 .nav").fadeIn(100);
        $("#sequence-theme2 .nav li:nth-child("+(sequence2.settings.startingFrameID)+") img").addClass("active");
    }
     sequence3.afterLoaded = function() {
        $("#sequence-theme3 .nav").fadeIn(100);
        $("#sequence-theme3 .nav li:nth-child("+(sequence3.settings.startingFrameID)+") img").addClass("active");
    }
     sequence4.afterLoaded = function() {
        $("#sequence-theme4 .nav").fadeIn(100);
        $("#sequence-theme4 .nav li:nth-child("+(sequence4.settings.startingFrameID)+") img").addClass("active");
    }
     sequence5.afterLoaded = function() {
        $("#sequence-theme5 .nav").fadeIn(100);
        $("#sequence-theme5 .nav li:nth-child("+(sequence5.settings.startingFrameID)+") img").addClass("active");
    }
     sequence6.afterLoaded = function() {
        $("#sequence-theme6 .nav").fadeIn(100);
        $("#sequence-theme6 .nav li:nth-child("+(sequence6.settings.startingFrameID)+") img").addClass("active");
    }
     sequence7.afterLoaded = function() {
        $("#sequence-theme7 .nav").fadeIn(100);
        $("#sequence-theme7 .nav li:nth-child("+(sequence7.settings.startingFrameID)+") img").addClass("active");
    }
    

    /*sequence1.beforeNextFrameAnimatesIn = function() {
        $("#sequence-theme1 .nav li:not(:nth-child("+(sequence1.nextFrameID)+")) img").removeClass("active");
        $("#sequence-theme1 .nav li:not(:nth-child("+(sequence1.nextFrameID)+"))").removeClass("active");
        $("#sequence-theme1 .nav li:nth-child("+(sequence1.nextFrameID)+") img").addClass("active");
        $("#sequence-theme1 .nav li:nth-child("+(sequence1.nextFrameID)+")").addClass("active");
    }*/
    sequence2.beforeNextFrameAnimatesIn = function() {
        $("#sequence-theme2 .nav li:not(:nth-child("+(sequence2.nextFrameID)+")) img").removeClass("active");
        $("#sequence-theme2 .nav li:not(:nth-child("+(sequence2.nextFrameID)+"))").removeClass("active");
        $("#sequence-theme2 .nav li:nth-child("+(sequence2.nextFrameID)+") img").addClass("active");
        $("#sequence-theme2 .nav li:nth-child("+(sequence2.nextFrameID)+")").addClass("active");
    }
    sequence3.beforeNextFrameAnimatesIn = function() {
        $("#sequence-theme3 .nav li:not(:nth-child("+(sequence3.nextFrameID)+")) img").removeClass("active");
        $("#sequence-theme3 .nav li:not(:nth-child("+(sequence3.nextFrameID)+"))").removeClass("active");
        $("#sequence-theme3 .nav li:nth-child("+(sequence3.nextFrameID)+") img").addClass("active");
        $("#sequence-theme3 .nav li:nth-child("+(sequence3.nextFrameID)+")").addClass("active");
    }
    sequence4.beforeNextFrameAnimatesIn = function() {
        $("#sequence-theme4 .nav li:not(:nth-child("+(sequence4.nextFrameID)+")) img").removeClass("active");
        $("#sequence-theme4 .nav li:not(:nth-child("+(sequence4.nextFrameID)+"))").removeClass("active");
        $("#sequence-theme4 .nav li:nth-child("+(sequence4.nextFrameID)+") img").addClass("active");
        $("#sequence-theme4 .nav li:nth-child("+(sequence4.nextFrameID)+")").addClass("active");
    }
    sequence5.beforeNextFrameAnimatesIn = function() {
        $("#sequence-theme5 .nav li:not(:nth-child("+(sequence5.nextFrameID)+")) img").removeClass("active");
        $("#sequence-theme5 .nav li:not(:nth-child("+(sequence5.nextFrameID)+"))").removeClass("active");
        $("#sequence-theme5 .nav li:nth-child("+(sequence5.nextFrameID)+") img").addClass("active");
        $("#sequence-theme5 .nav li:nth-child("+(sequence5.nextFrameID)+")").addClass("active");
    }
    sequence6.beforeNextFrameAnimatesIn = function() {
        $("#sequence-theme6 .nav li:not(:nth-child("+(sequence6.nextFrameID)+")) img").removeClass("active");
        $("#sequence-theme6 .nav li:not(:nth-child("+(sequence6.nextFrameID)+"))").removeClass("active");
        $("#sequence-theme6 .nav li:nth-child("+(sequence6.nextFrameID)+") img").addClass("active");
        $("#sequence-theme6 .nav li:nth-child("+(sequence6.nextFrameID)+")").addClass("active");
    }
    sequence7.beforeNextFrameAnimatesIn = function() {
        $("#sequence-theme7 .nav li:not(:nth-child("+(sequence7.nextFrameID)+")) img").removeClass("active");
        $("#sequence-theme7 .nav li:not(:nth-child("+(sequence7.nextFrameID)+"))").removeClass("active");
        $("#sequence-theme7 .nav li:nth-child("+(sequence7.nextFrameID)+") img").addClass("active");
        $("#sequence-theme7 .nav li:nth-child("+(sequence7.nextFrameID)+")").addClass("active");
    }
    
    /*$("#sequence-theme1").on("click", ".nav li", function() {
        $(this).children("img").removeClass("active").children("img").addClass("active");
        sequence1.nextFrameID = $(this).index()+1;
        sequence1.goTo(sequence1.nextFrameID);
    });*/
    $("#sequence-theme2").on("click", ".nav li", function() {
        $(this).children("img").removeClass("active").children("img").addClass("active");
        sequence2.nextFrameID = $(this).index()+1;
        sequence2.goTo(sequence2.nextFrameID);
    });
    $("#sequence-theme3").on("click", ".nav li", function() {
        $(this).children("img").removeClass("active").children("img").addClass("active");
        sequence3.nextFrameID = $(this).index()+1;
        sequence3.goTo(sequence3.nextFrameID);
    });
    $("#sequence-theme4").on("click", ".nav li", function() {
        $(this).children("img").removeClass("active").children("img").addClass("active");
        sequence4.nextFrameID = $(this).index()+1;
        sequence4.goTo(sequence4.nextFrameID);
    });
    $("#sequence-theme5").on("click", ".nav li", function() {
        $(this).children("img").removeClass("active").children("img").addClass("active");
        sequence5.nextFrameID = $(this).index()+1;
        sequence5.goTo(sequence5.nextFrameID);
    });
    $("#sequence-theme6").on("click", ".nav li", function() {
        $(this).children("img").removeClass("active").children("img").addClass("active");
        sequence6.nextFrameID = $(this).index()+1;
        sequence6.goTo(sequence6.nextFrameID);
    });
    $("#sequence-theme7").on("click", ".nav li", function() {
        $(this).children("img").removeClass("active").children("img").addClass("active");
        sequence7.nextFrameID = $(this).index()+1;
        sequence7.goTo(sequence7.nextFrameID);
    });
    /*$("#sequence-theme1").on("click", ".nav li:nth-child(4)", function() {
        $(this).removeClass("active").addClass("active");
        sequence1.nextFrameID = $(this).index()+1;
        sequence1.goTo(sequence1.nextFrameID);
    });*/
    $("#sequence-theme2").on("click", ".nav li:nth-child(4)", function() {
        $(this).removeClass("active").addClass("active");
        sequence2.nextFrameID = $(this).index()+1;
        sequence2.goTo(sequence2.nextFrameID);
    });
    $("#sequence-theme3").on("click", ".nav li:nth-child(4)", function() {
        $(this).removeClass("active").addClass("active");
        sequence3.nextFrameID = $(this).index()+1;
        sequence3.goTo(sequence3.nextFrameID);
    });
    $("#sequence-theme4").on("click", ".nav li:nth-child(4)", function() {
        $(this).removeClass("active").addClass("active");
        sequence4.nextFrameID = $(this).index()+1;
        sequence4.goTo(sequence4.nextFrameID);
    });
    $("#sequence-theme5").on("click", ".nav li:nth-child(4)", function() {
        $(this).removeClass("active").addClass("active");
        sequence5.nextFrameID = $(this).index()+1;
        sequence5.goTo(sequence5.nextFrameID);
    });
    $("#sequence-theme6").on("click", ".nav li:nth-child(4)", function() {
        $(this).removeClass("active").addClass("active");
        sequence6.nextFrameID = $(this).index()+1;
        sequence6.goTo(sequence6.nextFrameID);
    });
    $("#sequence-theme7").on("click", ".nav li:nth-child(4)", function() {
        $(this).removeClass("active").addClass("active");
        sequence7.nextFrameID = $(this).index()+1;
        sequence7.goTo(sequence7.nextFrameID);
    });
});